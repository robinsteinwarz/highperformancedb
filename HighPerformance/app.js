var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var salesManRouter = require('./routes/SalesManRoutes');
var salesManRecordRouter = require('./routes/SalesManRecordRoutes');
var authenticationRouter = require('./routes/AuthenticationRoutes');

var app = express();
var cors = require('cors');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// use it before all route definitions
app.use(cors({origin: '*'}));

app.use('/', indexRouter);
app.use('/SalesMan', salesManRouter);
app.use('/SalesManRecord', salesManRecordRouter);
app.use('/Authentication', authenticationRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// enable body parsing
const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: '1mb' }));

var config = {
  appRoot: __dirname // required config
};

module.exports = app;
