var express = require('express');
var router = express.Router();

var userController = require( '../src/Controller/UserController' );

router.post('/Auth/CreateUser', function (req, res, next) {
    userController.registerUser(req).then(result => {
        if(result === false){
            res.writeHead(422, {'Content-Type': 'text/plain'});
            res.write("Failed to create user. Submitted data was incomplete.");
        }else{
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write("Successfully created user.");
        }
        res.end();
    });
});

router.post('/Auth', function (req, res, next) {
    userController.isUserPasswordValid(req).then(result => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(result));
        res.end();
    });
});

module.exports = router;
