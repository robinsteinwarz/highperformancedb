const express = require('express');
const router = express.Router();

const recordController = require('../src/Controller/SalesManRecordController')

// GET SalesMan Details
router.get('/SalesManDetails/:id', function (req, res, next) {
    recordController.receiveSalesManDetails(req.params.id).then(result => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(result));
        res.end();
    })
});

router.post('/SalesManDetails/Social/:id', function (req, res, next) {
    recordController.saveSocialPerformance(req.params.id, req).then(r => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end();
    });
});

router.post('/SalesManDetails/Order/:id', function (req, res, next) {
    recordController.saveOrderBonus(req.params.id, req).then(r => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end();
    });
});

router.post('/SalesManDetails/Feedback/:id', function (req, res, next) {
    recordController.saveFeedback(req.params.id, req).then(r => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end();
    });
});

router.post('/SalesManDetails/Approve/:id', function (req, res, next) {
    recordController.approveRecord(req.params.id, req).then(r => {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end();
    });
});

module.exports = router;
