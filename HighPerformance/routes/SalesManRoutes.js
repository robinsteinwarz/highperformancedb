var express = require('express');
var router = express.Router();

const salesManController = require('../src/Controller/SalesManController');

// GET all SalesMan
router.get('/', function(req, res, next) {
    salesManController.getAllSalesMan().then(result => {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify([...result.values()]));
        res.end();
    }).catch(error => {
        console.log(error);
    });
});

// GET one SalesMan
router.get('/:id', function(req, res, next) {
    salesManController.getOneSalesMan(req.params.id).then(result => {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify(result));
        res.end();
    }).catch(error => {
        console.log(error);
    });
});

module.exports = router;
