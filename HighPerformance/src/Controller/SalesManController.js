const orangeHRM = require('../legacy/OrangeHRM/OrangeHRMConnector');

async function getAllSalesMan() {
    return await orangeHRM.getOrangeHRMAllSalesMan();
}

async function getOneSalesMan(salesManId) {
    return await orangeHRM.getOrangeHRMOneSalesMan(salesManId);
}

exports.getAllSalesMan = getAllSalesMan;
exports.getOneSalesMan = getOneSalesMan;
