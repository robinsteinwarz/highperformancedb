openCRX = require('../legacy/OpenCRX/OpenCRXAdapter');
orangeHRM = require('../legacy/OrangeHRM/OrangeHRMConnector');
smRecord = require('../database/SalesManRecordDAO');
bonusCalculator = require('../services/BonusCalculationService');

async function receiveSalesManDetails(salesManId) {
    // Patch SalesMan Records
    await applyRemoteChanges(salesManId);
    // Return definitly up-to-date performance records
    return smRecord.getSalesManRecords(salesManId);
}

async function saveSocialPerformance(salesManId, request) {
    let year = request.body.year;
    let socialId = request.body.socialId;
    let targetValue = request.body.targetValue;
    let actualValue = request.body.actualValue;
    let bonus = request.body.bonus;

    await smRecord.updateSocialPerformance(salesManId, year, socialId, targetValue, actualValue, bonus);
}

async function saveOrderBonus(salesManId, request) {
    let year = request.body.year;
    let orderId = request.body.orderId;
    let bonus = request.body.bonus;

    await smRecord.updateOrderBonus(salesManId, year, orderId, bonus);
}

async function saveFeedback(salesManId, request) {
    let year = request.body.year;
    let feedback = request.body.feedback;

    await smRecord.saveFeedback(salesManId, year, feedback);
}

async function approveRecord(salesManId, request) {
    let year = request.body.year;
    let orangeHRMId = request.body.orangeHRMId;
    let bonusSalary = await this.getTotalBonus(salesManId, year);

    await orangeHRM.saveSalesManBonusSalary(orangeHRMId, year, bonusSalary);
    await smRecord.setRecordApproval(salesManId, year, true);
}

async function getTotalBonus(salesManId, year){
    let localRecord = await smRecord.getSalesManRecords(salesManId);
    let bonus = 0;

    for(let record of localRecord.records){
        if(record.year === year){
            for(let order of record.orders){
                bonus += order.bonus;
            }

            for(let social of record.social){
                bonus += social.bonus;
            }
        }
    }

    return bonus;
}

async function applyRemoteChanges(salesManId) {
    // Retrieve local and foreign data
    const fetchedContent = await openCRX.getSalesManDetails(salesManId);
    const localContent = await smRecord.getSalesManRecords(salesManId);

    // Create Basic SalesMan Record if not present
    const salesManExists = await smRecord.salesManRecordExists(salesManId);
    if (!salesManExists) {
        const orangeHRMId = await orangeHRM.getOrangeHRMSalesManIdZuEmployeeId(salesManId);
        await smRecord.createSalesManRecord(salesManId, orangeHRMId);
    }

    // Loop through all years where an order exists
    for (let year of Object.keys(fetchedContent.sales)) {

        const salesManYearExists = await smRecord.salesManRecordYearExists(salesManId, year);
        if (!salesManYearExists) {
            await smRecord.createSalesManRecordYear(salesManId, year);
        }

        let mergeRecord = innateRecordObject();
        mergeRecord.year = year;
        mergeRecord.feedback = '';
        mergeRecord.approved = false;
        mergeRecord.orders = [];
        mergeRecord.social = [];

        // Merge orders local with remote
        if (fetchedContent.sales !== undefined && fetchedContent.sales !== null) {
            // For each product
            for (let productName of Object.keys(fetchedContent.sales[year])) {
                let order = fetchedContent.sales[year][productName];
                let bonus = bonusCalculator.calculateOrderBonus(order.productSoldQuantity, order.productPriceEach, order.customerRanking);
                let bonusManuallySet = false;

                if(localContent !== null){
                    for (let localRecord of localContent.records) {
                        if (Number(year) === localRecord.year) {
                            for (let localOrder of localRecord.orders) {
                                if (localOrder.productName === productName) {
                                    if (localOrder.bonusManuallySet === true
                                        && localOrder.bonus !== null) {
                                        bonus = localOrder.bonus;
                                        bonusManuallySet = true;
                                    }
                                }
                            }
                        }
                    }
                }

                mergeRecord.orders.push({
                    productName: productName,
                    quantity: order.productSoldQuantity,
                    priceEach: order.productPriceEach,
                    customerName: order.customerName,
                    customerRanking: order.customerRanking,
                    bonus: bonus,
                    bonusManuallySet: bonusManuallySet
                });
            }
        }

        let setLocalRecord = false;

        if (localContent !== null && localContent.records !== undefined && localContent.records !== null) {
            // Receive social local
            for (let localRecord of localContent.records) {
                if (localRecord.year === Number(year)) {
                    setLocalRecord = true;
                    mergeRecord.feedback = localRecord.feedback;
                    mergeRecord.social = localRecord.social;
                    if(localRecord.approved === undefined || localRecord.approved === null){
                        mergeRecord.approved = false;
                    }else{
                        mergeRecord.approved = localRecord.approved;
                    }
                    for (let competence of mergeRecord.social) {
                        if (competence.bonusManuallySet === false || competence.bonusManuallySet === undefined) {
                            competence.bonus = bonusCalculator.calculateSocialBonus(competence.actualValue);
                        }
                    }
                }
            }
        }

        if (!setLocalRecord) {
            mergeRecord.social = [
                {
                    competence: "leadership",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                },
                {
                    competence: "openness",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                },
                {
                    competence: "social behaviour",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                },
                {
                    competence: "attitude",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                },
                {
                    competence: "communication",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                },
                {
                    competence: "integrity",
                    targetValue: 1,
                    actualValue: 1,
                    bonus: 0,
                    bonusManuallySet: false
                }
            ]
        }

        await smRecord.updateSalesManRecord(salesManId, mergeRecord);
    }
}

function innateRecordObject(req) {
    let year = 0;
    let feedback = '';
    let orders = {};
    let social = {};

    if (req !== undefined) {
        year = req.body.year | 0;
        feedback = req.body.feedback | '';
        orders = req.body.orders | {};
        social = req.body.social | {};
    }

    let mongooseRecord = {};
    mongooseRecord.year = year;
    mongooseRecord.feedback = feedback;
    mongooseRecord.orders = orders;
    mongooseRecord.social = social;

    return mongooseRecord;
}


exports.receiveSalesManDetails = receiveSalesManDetails;
exports.saveSocialPerformance = saveSocialPerformance;
exports.saveOrderBonus = saveOrderBonus;
exports.saveFeedback = saveFeedback;
exports.approveRecord = approveRecord;
exports.getTotalBonus = getTotalBonus;
