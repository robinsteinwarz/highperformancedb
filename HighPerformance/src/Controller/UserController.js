const userStorage = require('../database/UserDAO');


async function isUserPasswordValid(req) {
    if(req.body.name === undefined || req.body.name === null || req.body.password === undefined || req.body.password === null){
        return false;
    }

    const name = req.body.name;
    const pw = req.body.password;

    return await userStorage.isUserPasswordValid(name, pw);
}

async function registerUser(req) {
    if(req.body.name === undefined || req.body.password === undefined || req.body.userType === undefined){
        return false;
    }

    const name = req.body.name;
    const pw = req.body.password;
    const userType = req.body.userType;
    let employeeId = -1;

    if(userType !== 'salesman' && userType !== 'ceo' && userType !== 'hr' && userType !== 'admin'){
        return false;
    }

    if(userType === 'salesman' && req.body.employeeId === undefined){
        return false;
    }else{
        employeeId = req.body.employeeId;
    }

    return await userStorage.registerUser(name, pw, userType, employeeId);
}


exports.isUserPasswordValid = isUserPasswordValid;
exports.registerUser = registerUser;
