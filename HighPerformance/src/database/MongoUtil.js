//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://127.0.0.1/performance';
mongoose.connect(mongoDB, {useNewUrlParser: true});

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//Define a schema
let schema = mongoose.Schema;

let salesManRecordSchema = new schema({
    salesManId: Number,
    orangeHRMId: Number,
    records: [{
        year: Number,
        feedback: String,
        approved: Boolean,
        orders: [{
            productName: String,
            quantity: Number,
            priceEach: Number,
            customerName: String,
            customerRanking: Number,
            bonus: Number,
            bonusManuallySet: Boolean
        }],
        social: [{
            competence: String,
            targetValue: Number,
            actualValue: Number,
            bonus: Number,
            bonusManuallySet: Boolean
        }]
    }]
});
const salesManRecord = mongoose.model('SalesManRecord', salesManRecordSchema);

let userSchema = new schema({
    name: String,
    password: String,
    userType: String,
    employeeId: String
});
const user = mongoose.model('User', userSchema);

