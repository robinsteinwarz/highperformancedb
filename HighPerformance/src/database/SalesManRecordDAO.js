const mongoose = require('mongoose');

// Init DB
const mongoUtil = require('./MongoUtil');
const mongoDB = require('mongodb');
const salesManRecord = mongoose.model('SalesManRecord');

async function getSalesManRecords(salesManId) {
    console.log("Get SalesMan Performance-Records");
    return JSON.parse(JSON.stringify(await salesManRecord.findOne({salesManId: salesManId})));
}

async function createSalesManRecord(salesManId, orangeHRMId) {
    console.log("Create Record for SalesMan");

    // create salesman base record
    new salesManRecord({
        salesManId: salesManId,
        orangeHRMId: orangeHRMId,
        feedback: '',
        approved: false,
        records: [],
        social: []
    }).save((error, result) => {
        logError(error);
        return result
    });
}

async function createSalesManRecordYear(salesManId, year) {
    console.log("Create Record year for SalesMan");

    salesManRecord.findOneAndUpdate({salesManId: salesManId}, {
            $push:
                {
                    "records": {
                        year: year
                    },
                }
        },
        {}, function (err, doc) {
            logError(err);
        });
}

async function updateSalesManRecord(salesManId, data) {
    console.log("Update SalesMan Record");

    await salesManRecord.findOneAndUpdate({salesManId: salesManId, "records.year": data.year}, {
            $set:
                {
                    "records.$.year": data.year,
                    "records.$.feedback": data.feedback,
                    "records.$.orders": data.orders,
                    "records.$.social": data.social,
                    "records.$.approved": data.approved,
                }
        },
        {}, function (err, doc) {
            logError(err);
        });
}

async function updateOrderBonus(salesManId, year, orderId, bonus) {
    if (bonus !== null && bonus !== "") {
        await salesManRecord.findOneAndUpdate({
                salesManId: salesManId
            }, {
                $set:
                    {
                        "records.$[outer].orders.$[inner].bonus": bonus,
                        "records.$[outer].orders.$[inner].bonusManuallySet": true
                    }
            },
            {
                "arrayFilters": [{
                    "outer.year": year,
                },
                    {
                        "inner._id": mongoDB.ObjectID(orderId),
                    }]
            }, function (err, doc) {
                logError(err);
            });
    } else {
        await salesManRecord.findOneAndUpdate({
                salesManId: salesManId
            }, {
                $set:
                    {
                        "records.$[outer].orders.$[inner].bonusManuallySet": false
                    }
            },
            {
                "arrayFilters": [{
                    "outer.year": year,
                },
                    {
                        "inner._id": mongoDB.ObjectID(orderId),
                    }]
            }, function (err, doc) {
                logError(err);
            });
    }

    this.setRecordApproval(salesManId, year, false);
}

async function updateSocialPerformance(salesManId, year, socialId, targetValue, actualValue, bonus) {
    if (bonus !== null && bonus !== "") {
        await salesManRecord.findOneAndUpdate({
                salesManId: salesManId
            }, {
                $set:
                    {
                        "records.$[outer].social.$[inner].targetValue": targetValue,
                        "records.$[outer].social.$[inner].actualValue": actualValue,
                        "records.$[outer].social.$[inner].bonus": bonus,
                        "records.$[outer].social.$[inner].bonusManuallySet": true
                    }
            },
            {
                "arrayFilters": [{
                    "outer.year": year,
                },
                    {
                        "inner._id": mongoDB.ObjectID(socialId),
                    }]
            }, function (err, doc) {
                logError(err);
            });
    } else {
        await salesManRecord.findOneAndUpdate({
                salesManId: salesManId
            }, {
                $set:
                    {
                        "records.$[outer].social.$[inner].targetValue": targetValue,
                        "records.$[outer].social.$[inner].actualValue": actualValue,
                        "records.$[outer].social.$[inner].bonusManuallySet": false
                    }
            },
            {
                "arrayFilters": [{
                    "outer.year": year,
                },
                    {
                        "inner._id": mongoDB.ObjectID(socialId),
                    }]
            }, function (err, doc) {
                logError(err);
            });
    }

    this.setRecordApproval(salesManId, year, false);
}

async function saveFeedback(salesManId, year, feedback) {

    await salesManRecord.findOneAndUpdate({
            salesManId: salesManId
        }, {
            $set:
                {
                    "records.$[outer].feedback": feedback
                }
        },
        {
            "arrayFilters": [{
                "outer.year": year,
            }]
        }, function (err, doc) {
            logError(err);
        });
}

async function setRecordApproval(salesManId, year, approval){
    console.log("Approve Salesman" + salesManId + " " + year + " " + approval);
    await salesManRecord.findOneAndUpdate({
            salesManId: salesManId
        }, {
            $set:
                {
                    "records.$[outer].approved": approval
                }
        },
        {
            "arrayFilters": [{
                "outer.year": year,
            }]
        }, function (err, doc) {
            logError(err);
        });
}


async function salesManRecordExists(salesManId) {
    const number = await salesManRecord.countDocuments({salesManId: salesManId});

    return number !== 0;
}

async function salesManRecordYearExists(salesManId, year) {
    const number = await salesManRecord.countDocuments({salesManId: salesManId, "records.year": year});

    return number !== 0;
}

const logError = (error, result) => {
    if (error) {
        console.log(error);
    }
    if (result != null) {
        console.log(result);
    }
};

exports.getSalesManRecords = getSalesManRecords;
exports.createSalesManRecord = createSalesManRecord;
exports.updateSalesManRecord = updateSalesManRecord;
exports.salesManRecordExists = salesManRecordExists;
exports.salesManRecordYearExists = salesManRecordYearExists;
exports.createSalesManRecordYear = createSalesManRecordYear;
exports.updateSocialPerformance = updateSocialPerformance;
exports.updateOrderBonus = updateOrderBonus;
exports.saveFeedback = saveFeedback;
exports.setRecordApproval = setRecordApproval;
