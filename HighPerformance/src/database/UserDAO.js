const mongoose = require('mongoose');

// Init DB
var mongoUtil = require('./MongoUtil');
var md5 = require('md5');
const salt = '(J:13@55Dx+W'
const user = mongoose.model('User');

// Get UserPW
async function isUserPasswordValid(name, password) {
    console.log("User Password Validation");
    return user.findOne({name: name}).then(result => {
        logError(result);
        if(result === null){
            return false;
        }

        if (md5(password + salt) === result.password) {
            return {
                userType: result.userType,
                employeeId: result.employeeId
            };
        }

        return false;
    });
}

async function registerUser(name, password, userType, employeeId) {
    console.log("User creation");

    if (name === '' || name === undefined || password === '' || password === undefined || userType === '' || userType === undefined) {
        console.log("User was not created duo to missing information");
        return false;
    }

    // create salesman base record
    new user({
        name: name,
        password: md5(password + salt),
        userType: userType,
        employeeId: employeeId
    }).save((error, result) => {
        logError(error);
        return result
    });
}

const logError = (error, result) => {
    if (error) {
        console.log(error);
    }
    if (result != null) {
        console.log(result);
    }
};

exports.isUserPasswordValid = isUserPasswordValid;
exports.registerUser = registerUser;
