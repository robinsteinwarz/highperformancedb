openCRXConnector = require('./OpenCRXConnector');


async function getSalesManDetails(employeeId) {
    const salesOrders = await openCRXConnector.getAllSalesOrders();

    let salesManDetails = {};
    salesManDetails.salesManId = employeeId;
    salesManDetails.sales = {};
    for (const order of salesOrders) {
        const orderId = order.identity.replace('xri://@openmdx*org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/', '');
        const salesManId = order.salesRep.$.replace('xri://@openmdx*org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/', '');
        const customerId =  order.customer.$.replace('xri://@openmdx*org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/', '');

        const governmentId = await getOpenCRXSalesManId(salesManId);
        const positions = await getPositions(orderId);
        const customerDetails = await getCustomerDetails(customerId);

        if (employeeId === governmentId) {
            // Sales
            for (const position of positions) {
                if (salesManDetails.sales[position.sellingYear] === undefined) {
                    salesManDetails.sales[position.sellingYear] = {};
                }
                salesManDetails.sales[position.sellingYear][position.productName] = {
                    // https://stackoverflow.com/questions/3612744/remove-insignificant-trailing-zeros-from-a-number
                    productSoldQuantity: Number(position.productSoldQuantity).toFixed(0),
                    productPriceEach: Number(position.productPriceEach).toFixed(0),
                    customerName: customerDetails.customerName,
                    customerRanking: customerDetails.customerRanking,
                };
            }
        }
    }
    return salesManDetails;
}

async function getPositions(id) {
    const positions = await openCRXConnector.getPositionOfSalesOrder(id);

    // no positions available
    if (!positions) {
        return;
    }

    const positionDetails = [];
    for (const position of positions) {
        const productId = position.product['@href'].replace('https://sepp-crm.inf.h-brs.de/opencrx-rest-CRX/org.opencrx.kernel.product1/provider/CRX/segment/Standard/product/', '');
        const productName = await openCRXConnector.getProduct(productId).then(result =>
            result.data.name
        );

        positionDetails.push({
            productSoldQuantity: position.quantity,
            productPriceEach: position.amount,
            productName: productName,
            sellingYear: new Date(position.createdAt).getFullYear()
        })
    }

    return positionDetails;
}

async function getCustomerDetails(id) {
    const customer = await openCRXConnector.getAccountById(id);

    return {
        customerName: customer.fullName,
        customerRanking: customer.accountRating
    }
}

async function getOpenCRXSalesManId(id) {
    const salesMan = await openCRXConnector.getAccountById(id);
    return "" + salesMan.governmentId;
}

exports.getSalesManDetails = getSalesManDetails;
