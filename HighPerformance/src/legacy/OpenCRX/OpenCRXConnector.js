const axios = require('axios');
const kafkaService = require('../../services/KafkaService');

const baseUrl = 'https://sepp-crm.inf.h-brs.de/opencrx-rest-CRX';
const credentials = {
    username: 'guest',
    password: 'guest',
};

const config = {
    headers: {
        'Accept': 'application/json'
    },
    auth: credentials,
};

async function getAllSalesOrders(){
    return await axios.get(`${baseUrl}/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder`, config)
        .then(result => {
            if(result.data.objects === undefined){
                return [];
            }
            return result.data.objects;
        })
}

async function getAccountById(id){
    return await axios.get(`${baseUrl}/org.opencrx.kernel.account1/provider/CRX/segment/Standard/account/${id}`, config)
        .then(result => {
            return result.data;
        })
}

async function getPositionOfSalesOrder(id){
    return await axios.get(`${baseUrl}/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/${id}/position`, config)
        .then(result => {
            if(result.data.objects === undefined){
                return [];
            }
            return result.data.objects;
        })
}

async function getProduct(id){
    return await axios.get(`${baseUrl}/org.opencrx.kernel.product1/provider/CRX/segment/Standard/product/${id}`, config)
        .then(result => {
            return result;
        })
}

exports.getAllSalesOrders = getAllSalesOrders;
exports.getAccountById = getAccountById;
exports.getPositionOfSalesOrder = getPositionOfSalesOrder;
exports.getProduct = getProduct;
