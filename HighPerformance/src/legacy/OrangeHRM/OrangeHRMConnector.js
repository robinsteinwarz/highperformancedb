const axios = require('axios');
const querystring = require('querystring');
const kafkaService = require('../../services/KafkaService');

let accessToken;
let config = {};
const baseURL = 'https://sepp-hrm.inf.h-brs.de/symfony/web/index.php';

async function getOrangeHRMToken(){
    const tokenData = {
        client_id: 'api_oauth_id',
        client_secret: 'oauth_secret',
        grant_type: 'password',
        username: 'Steinwarz',
        password: 'T1S!,SPasFXs'
    };

    const response = axios.post(`${baseURL}/oauth/issueToken`, tokenData )
        .then(res => {
            accessToken = res.data['access_token'];
            config.headers = {
                'Authorization':  `Bearer ${accessToken}`,
                'Content-Type': 'application/x-www-form-urlencoded',
                Accept: 'application/json'
            }
        })
        .catch((error) => { errorPrinter(error); });

    return await response;
}

async function getOrangeHRMAllSalesMan(){
    await checkOrangeHRMToken();

    const res = await axios.get(`${baseURL}/api/v1/employee/search`, config);
    resErrorPrinter(res);

    let salesmen = new Set();
    for(const salesman of Object.values(res.data.data)){
        if(salesman.jobTitle === "Senior Salesman"){
            let smAdapter = {
                employeeId: salesman.code,
                fullName: salesman.fullName
            };

            salesmen.add(smAdapter);
        }
    }

    return salesmen;
}

async function getOrangeHRMSalesManIdZuEmployeeId(employeeId){
    await checkOrangeHRMToken();

    const res = await axios.get(`${baseURL}/api/v1/employee/search`, config);
    resErrorPrinter(res);

    let salesmenId;
    for(const salesman of Object.values(res.data.data)){
        if(salesman.jobTitle === "Senior Salesman" && salesman.code === employeeId){
            salesmenId = salesman.employeeId;
        }
    }

    return salesmenId;
}

async function getOrangeHRMOneSalesMan(salesManId){
    await checkOrangeHRMToken();

    const res = await axios.get(`${baseURL}/api/v1/employee/search`, config);
    resErrorPrinter(res);

    for(const salesman of Object.values(res.data.data)){
        if(salesman.code === salesManId){
            return {
                employeeId: salesman.code,
                fullName: salesman.fullName
            };
        }
    }

    return null;
}

async function saveSalesManBonusSalary(orangeHRMId, year, bonusSalary){
    await checkOrangeHRMToken();

    let res = await axios.post(`${baseURL}/api/v1/employee/${orangeHRMId}/bonussalary`, querystring.stringify({
        year: year,
        value: bonusSalary.toFixed(0)
    }), config).catch((error) => { errorPrinter(error); });
    resErrorPrinter(res);

    return res;
}

async function checkOrangeHRMToken(){
    // Make sure orange Token has been received
    if(config.headers === undefined){
        await getOrangeHRMToken();
    }
}

function resErrorPrinter(res){
    if(res !== undefined && res.data !== undefined && res.data.error !== undefined){
        console.log(res.data.error);
    }
}

function errorPrinter(error){
    // CATCH as https://github.com/axios/axios#handling-errors states
    if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    } else if (error.request) {
        console.log(error.request);
    } else {
        console.log('Error', error.message);
    }
    console.log(error.config);
}

exports.getOrangeHRMAllSalesMan = getOrangeHRMAllSalesMan;
exports.getOrangeHRMOneSalesMan = getOrangeHRMOneSalesMan;
exports.saveSalesManBonusSalary = saveSalesManBonusSalary;
exports.getOrangeHRMSalesManIdZuEmployeeId = getOrangeHRMSalesManIdZuEmployeeId;
