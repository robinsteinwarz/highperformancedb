

function calculateOrderBonus(quantity, priceEach, customerRanking){
    return Number(quantity * (priceEach * .015) * customerRanking).toFixed(0);
}

function calculateSocialBonus(actualValue){
    if(actualValue < 1 || actualValue > 5){
        return 0;
    }

    return [0,5,20,50,100][actualValue-1];
}

exports.calculateOrderBonus = calculateOrderBonus;
exports.calculateSocialBonus = calculateSocialBonus;
