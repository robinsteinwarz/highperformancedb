const { Kafka } = require('kafkajs')

const kafka = new Kafka({
    clientId: 'highperformance',
    brokers: ['sepp-kafka.inf.h-brs.de:9092']
});

async function pushLegacyMessage(msg){

    const producer = kafka.producer();

    await producer.connect();

    await producer.send({
        topic: 'access to enterprise systems',
        messages: [
            { value: msg },
        ],
    });

    await producer.disconnect()
}

exports.pushLegacyMessage = pushLegacyMessage;
