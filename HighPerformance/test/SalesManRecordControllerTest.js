var sinon = require("sinon");
var assert =  require("chai").assert;
var smRecordC = require("../src/Controller/SalesManRecordController");
const SMRecordDAO = require("../src/database/SalesManRecordDAO");

describe('Ensure correct total bonus calculation', () => {
    it('bonus calculation', async () => {
        let recordDAOStub = sinon.stub(SMRecordDAO, 'getSalesManRecords').callsFake(() => {
            return {
                salesManId: "90124",
                records: [{
                    year: 2020,
                    orders: [
                        {
                            productName: "A",
                            quantity: 10,
                            priceEach: 2713,
                            customerRanking: 1,
                            bonus: 400
                        },
                        {
                            productName: "B",
                            quantity: 15,
                            priceEach: 3255,
                            customerRanking: 1,
                            bonus: 700
                        }],
                    social: [
                        {
                            actualValue: 5,
                            bonus: 100
                        },
                        {
                            actualValue: 3,
                            bonus: 20
                        }]
                }]
            }
        });

        let bonus = await smRecordC.getTotalBonus(90124, 2020);
        sinon.assert.calledOnce(recordDAOStub);
        assert.equal(bonus, 1220);
        recordDAOStub.restore();
    })
});

