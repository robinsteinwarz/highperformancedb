import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public auth: AuthenticationService;

  constructor(auth: AuthenticationService) {
    this.auth = auth;
  }

  ngOnInit(): void {
  }

  login(): void {
    // @ts-ignore
    const name = document.getElementById('username').value;
    // @ts-ignore
    const password = document.getElementById('password').value;

    this.auth.authenticate(name, password);
  }

}
