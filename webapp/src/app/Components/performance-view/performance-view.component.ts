import {Component, ViewChild, NgModule, OnInit} from '@angular/core';
import {SalesManDetailViewComponent} from '../sales-man-detail-view/sales-man-detail-view.component';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-performance-view',
  templateUrl: './performance-view.component.html',
  styleUrls: ['./performance-view.component.css']
})
@ViewChild(SalesManDetailViewComponent)
export class PerformanceViewComponent implements OnInit {
  salesManId = -1;

  public auth: AuthenticationService;

  constructor(auth: AuthenticationService) {
    this.auth = auth;

    if (auth.isSalesman()) {
      this.salesManId = Number(auth.getEmployeeId());
    }
  }

  onSelectedSalesMan(id: number): void {
    this.salesManId = id;
  }

  ngOnInit(): void {
  }

}
