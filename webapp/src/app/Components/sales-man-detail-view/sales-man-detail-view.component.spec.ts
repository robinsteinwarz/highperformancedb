import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesManDetailViewComponent } from './sales-man-detail-view.component';

describe('SalesManDetailViewComponent', () => {
  let component: SalesManDetailViewComponent;
  let fixture: ComponentFixture<SalesManDetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesManDetailViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesManDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
