import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {SalesManService} from '../services/sales-man.service';
import {PerformanceRecord} from '../performance-record';
import {SalesMan} from '../sales-man';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-sales-man-detail-view',
  templateUrl: './sales-man-detail-view.component.html',
  styleUrls: ['./sales-man-detail-view.component.css']
})
export class SalesManDetailViewComponent implements OnInit, OnChanges {

  performanceRecord: PerformanceRecord | undefined;
  loading = false;
  salesMan: SalesMan | undefined;
  @Input() salesManClassService: SalesManService;
  @Input() salesManId = -1;

  public auth: AuthenticationService;

  orderAdaption = false;
  orderAdaptionId = -1;

  feedbackAdaption = false;

  adaption = false;
  adaptionId = -1;
  socialSelect = [
    'bad',
    'expandable',
    'okay',
    'good',
    'excellent'
  ];

  constructor(private salesManService: SalesManService, auth: AuthenticationService) {
    this.salesManClassService = salesManService;
    this.auth = auth;
  }

  initSalesManDetailsPage(id: number): void {
    this.loading = true;
    this.toggleOrderAdaptation(-1);
    this.toggleCompetenceAdaptation(-1);

    Promise.all([
      this.salesManClassService.getSalesManDetails(id).toPromise().then(performanceRecord => {
        this.performanceRecord = performanceRecord;
        console.log(this.performanceRecord);
      }),
      this.salesManClassService.getOneSalesMan(String(id)).toPromise().then(salesMan => {
        this.salesMan = salesMan;
      })
    ]).then(() => {
      this.loading = false;
    });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.salesManId = changes.salesManId.currentValue;

    if (this.salesManId !== -1) {
      this.initSalesManDetailsPage(changes.salesManId.currentValue);
    }
  }

  toggleCompetenceAdaptation(id: number): void {
    if ((id === this.adaptionId || id === -1) && this.adaption) {
      this.adaption = false;
      return;
    }
    this.adaption = true;
    this.adaptionId = id;
  }

  toggleOrderAdaptation(id: number): void {
    if ((id === this.orderAdaptionId || id === -1) && this.orderAdaption) {
      this.orderAdaption = false;
      return;
    }
    this.orderAdaption = true;
    this.orderAdaptionId = id;
  }

  saveAdaption(year: number, socialId: string): void {
    // @ts-ignore
    const targetValue = document.getElementById('target-value-' + year + '-' + this.adaptionId).value;
    // @ts-ignore
    const actualValue = document.getElementById('actual-value-' + year + '-' + this.adaptionId).value;
    // @ts-ignore
    const bonus = document.getElementById('bonus-' + year + '-' + this.adaptionId).value;

    this.salesManClassService.saveSocialPerformance(this.salesManId, year, socialId, targetValue, actualValue, bonus)
      .subscribe(_ => {
        if (this.performanceRecord === undefined) {
          return;
        }

        for (const record of this.performanceRecord.records) {
          if (record.year === year) {
            for (const social of record.social) {
              if (social._id === socialId) {
                social.actualValue = actualValue;
                social.targetValue = targetValue;
                if (bonus !== undefined && bonus !== null && bonus !== '') {
                  social.bonus = bonus;
                  social.bonusManuallySet = true;
                } else {
                  social.bonus = this.calculateSocialBonus(actualValue);
                }
              }
            }
          }
        }
      });
    this.toggleCompetenceAdaptation(this.adaptionId);
  }

  saveOrderAdaption(year: number, orderId: string): void {
    // @ts-ignore
    let bonus = document.getElementById('order-bonus-' + year + '-' + this.orderAdaptionId).value;

    if (bonus === undefined || bonus === '') {
      bonus = null;
    }

    this.salesManClassService.saveOrderBonusPerformance(this.salesManId, year, orderId, bonus)
      .subscribe(_ => {
        if (this.performanceRecord === undefined) {
          return;
        }

        for (const record of this.performanceRecord.records) {
          if (record.year === year) {
            for (const order of record.orders) {
              if (order._id === orderId) {
                if (bonus === null) {
                  order.bonus = Number(this.calculateOrderBonus(Number(order.quantity),
                    Number(order.priceEach), Number(order.customerRanking)));
                } else {
                  order.bonus = bonus;
                }
              }
            }
          }
        }
      });

    this.toggleOrderAdaptation(this.orderAdaptionId);
  }

  approveRecord(year: number): void {
    // click hack
    this.toggleDisplayPerformanceDetails('performance-record-details-' + year);

    if (this.performanceRecord === undefined) {
      return;
    }

    this.salesManClassService.approveRecord(this.salesManId, this.performanceRecord.orangeHRMId, year).subscribe(_ => {
      if (this.performanceRecord === undefined) {
        return;
      }

      for (const record of this.performanceRecord.records) {
        if (record.year === year) {
          record.approved = true;
        }
      }

    });
  }

  feedbackChanged(): void {
    this.feedbackAdaption = true;
  }

  saveFeedback(year: number): void {
    // @ts-ignore
    const feedback = document.getElementById('feedback-' + year).value;

    this.salesManClassService.saveFeedback(this.salesManId, year, feedback)
      .subscribe(_ => {
        if (this.performanceRecord === undefined) {
          return;
        }

        for (const record of this.performanceRecord.records) {
          if (record.year === year) {
            record.feedback = feedback;
          }
        }
      });

    this.feedbackAdaption = false;
  }

  calculateSubEvaluationTotalBonus(orders: any): number {
    if (orders === undefined || orders === null) {
      return 0;
    }
    let bonus = 0;
    for (const order of Object.values(orders)) {
      // @ts-ignore
      if (order.bonus !== undefined) {
        // @ts-ignore
        bonus += Number(order.bonus);
      }
    }

    return bonus;
  }

  calculateEvaluationTotalBonus(performance: any): number {
    return Number(this.calculateSubEvaluationTotalBonus(performance.orders)) +
      Number(this.calculateSubEvaluationTotalBonus(performance.social));
  }

  mapRanking(id: number): string {
    if (id < 1 || id > 5) {
      return 'bad';
    }

    return ['bad', 'expandable', 'okay', 'good', 'excellent'][id - 1];
  }

  mapClientRanking(id: number): string {
    if (id < 1 || id > 5) {
      return 'okay';
    }

    return ['excellent', 'very good', 'good', 'negotiable', 'ok'][id - 1];
  }

  calculateSocialBonus(actualValue: number): number {
    if (actualValue < 1 || actualValue > 5) {
      return 0;
    }

    return [0, 5, 20, 50, 100][actualValue - 1];
  }

  calculateOrderBonus(quantity: number, priceEach: number, customerRanking: number): string {
    return (quantity * (priceEach * .015) * customerRanking).toFixed(0);
  }

  toggleDisplayPerformanceDetails(id: string): void {
    const element = document.getElementById(id);

    if (element === null) {
      return;
    }

    (element.style.display === 'none' || element.style.display === '') ? element.style.display = 'block' : element.style.display = 'none';
  }

}
