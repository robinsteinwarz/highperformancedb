import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesManComponent } from './sales-man.component';

describe('SalesManComponent', () => {
  let component: SalesManComponent;
  let fixture: ComponentFixture<SalesManComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesManComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesManComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
