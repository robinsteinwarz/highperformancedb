import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SalesMan} from '../sales-man';
import {SalesManService} from '../services/sales-man.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-sales-man',
  templateUrl: './sales-man.component.html',
  styleUrls: ['./sales-man.component.css']
})
export class SalesManComponent implements OnInit {

  salesMan: Observable<SalesMan[]>;
  filteredSalesMan: SalesMan[] = [];
  salesManFilter = '';
  @Output() selectedSalesMan = new EventEmitter<number>();

  constructor(private salesManService: SalesManService) {
    this.salesMan = salesManService.getSalesMan();
    this.filterChanged();
  }

  filterChanged(): void {
    let salesMen: SalesMan[] = [];

    this.salesMan.subscribe(item => {
      salesMen = item;

      if (this.salesManFilter === undefined || this.salesManFilter === '') {
        this.filteredSalesMan = salesMen;
        return;
      }

      this.filteredSalesMan = salesMen.filter(salesMan => salesMan.fullName.toLocaleLowerCase()
        .indexOf(this.salesManFilter.toLocaleLowerCase()) !== -1);
    });
  }

  selectedSalesManChanged(id: number): void {
    this.selectedSalesMan.emit(id);
  }


  ngOnInit(): void {
  }

}
