import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PerformanceViewComponent} from './performance-view/performance-view.component';

const routes: Routes = [
  { path: '', component: PerformanceViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
