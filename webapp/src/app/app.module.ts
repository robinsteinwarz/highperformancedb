import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SalesManComponent} from './sales-man/sales-man.component';
import {HttpClientModule} from '@angular/common/http';
import {SalesManDetailViewComponent} from './sales-man-detail-view/sales-man-detail-view.component';
import {PerformanceViewComponent} from './performance-view/performance-view.component';
import {FormsModule} from '@angular/forms';
import {AuthComponent} from './auth/auth.component';

@NgModule({
  declarations: [
    AppComponent,
    SalesManComponent,
    SalesManDetailViewComponent,
    PerformanceViewComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
