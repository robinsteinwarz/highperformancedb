export interface PerformanceRecord {
  salesManId: number;
  orangeHRMId: number;
  records: [{
    year: number;
    feedback: string;
    approved: boolean;
    orders: [{
      _id: string,
      productName: string;
      quantity: string;
      priceEach: string;
      customerName: string;
      customerRanking: number;
      bonus: number,
      bonusManuallySet: boolean
    }];
    social: [{
      _id: string,
      competence: string,
      targetValue: number,
      actualValue: number,
      bonus: number,
      bonusManuallySet: boolean
    }];
  }];
}

