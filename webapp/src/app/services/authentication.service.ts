import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private authURL = 'http://localhost:3000/Authentication';
  private authError = false;

  constructor(
    private http: HttpClient) {
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('userName') !== null;
  }

  isAdmin(): boolean {
    return localStorage.getItem('userType') === 'admin';
  }

  isCEO(): boolean {
    return localStorage.getItem('userType') === 'ceo';
  }

  isHR(): boolean {
    return localStorage.getItem('userType') === 'hr';
  }

  hasAuthenticationError(): boolean {
    return this.authError;
  }

  authenticate(name: string, password: string): void {
    if (name === '' || name === undefined || password === '' || password === undefined) {
      this.authError = true;
      return;
    }

    this.http.post(this.authURL + '/Auth', {
      name,
      password
    }).toPromise().then(result => {
      if (result === 'false' || result === false) {
        this.authError = true;
      } else {
        this.authError = false;
        localStorage.setItem('userName', name);
        localStorage.setItem('userType', '' + result);
      }
    });
  }

  logout(): void {
    this.authError = false;
    localStorage.removeItem('userName');
    localStorage.removeItem('userType');
  }

  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}
