import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {SalesMan} from 'src/app/sales-man';
import {PerformanceRecord} from '../performance-record';

@Injectable({
  providedIn: 'root'
})
export class SalesManService {

  private salesManURL = 'http://localhost:3000/SalesMan';
  private salesManDetailsURL = 'http://localhost:3000/SalesManRecord/SalesManDetails/';

  constructor(
    private http: HttpClient) {
  }

  getSalesMan(): Observable<SalesMan[]> {
    return this.http.get<SalesMan[]>(this.salesManURL)
      .pipe(
        catchError(this.handleError<SalesMan[]>('getSalesMan', []))
      );
  }

  getOneSalesMan(id: number): Observable<SalesMan> {
    return this.http.get<SalesMan>(this.salesManURL + '/' + id)
      .pipe(
        catchError(this.handleError<SalesMan>('getSalesMan'))
      );
  }

  getSalesManDetails(id: number): Observable<PerformanceRecord> {
    return this.http.get<PerformanceRecord>(this.salesManDetailsURL + id)
      .pipe(
        catchError(this.handleError<PerformanceRecord>('getPerformanceRecordsOfSalesman'))
      );
  }

  saveSocialPerformance(salesManId: number, year: number, socialId: string, targetValue: number, actualValue: number,
                        // tslint:disable-next-line:ban-types
                        bonus: number): Observable<Object> {
    return this.http.post(this.salesManDetailsURL + 'Social/' + salesManId, {
      year,
      socialId,
      targetValue,
      actualValue,
      bonus
    });
  }

  // tslint:disable-next-line:ban-types
  saveOrderBonusPerformance(salesManId: number, year: number, orderId: string, bonus: number): Observable<Object> {
    return this.http.post(this.salesManDetailsURL + 'Order/' + salesManId, {
      year,
      orderId,
      bonus
    });
  }

  // tslint:disable-next-line:ban-types
  saveFeedback(salesManId: number, year: number, feedback: string): Observable<Object> {
    return this.http.post(this.salesManDetailsURL + 'Feedback/' + salesManId, {
      year,
      feedback
    });
  }

  // tslint:disable-next-line:ban-types
  approveRecord(salesManId: number, orangeHRMId: number, year: number): Observable<Object> {
    return this.http.post(this.salesManDetailsURL + 'Approve/' + salesManId, {
      year,
      orangeHRMId
    });
  }

  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }

}
